# MEGA65 Jueguito X-mas 2021

## Objetivo

Hacer un jueguito para la [The Big MEGA65 Competition][mega65-itchio]

La mayoria de nosotros no sabemos nada de la MEGA65.
Y tambien es la primera vez que trabajamos juntos en un proyecto, y sumadado que arrancamos un mes
tarde, ponemos expectativas mas o menos realistas:

* Aprender un poco de la MEGA65
* Divertirnos mientras aprendemos
* Y cuanlquier otra cosa, bienvenida


[mega65-itchio]: https://itch.io/jam/the-big-mega65-christmas-competition

## Jueguito

* Para unirte al grupo, entra aca y listo: [chat](https://chat.rebelion.digital/channel/mega65-jueguito-xmas-compo-2021)

* Tareas a hacer y demas: [tareas pendientes](https://docs.google.com/spreadsheets/d/1kE2fkJZCq4OxJgCBpv5hocQ-eqLytROfbCt8PSC60Cw/edit#gid=0)

* Gitlab: Registrate una cuenta en [gitlab](https://gitlab.com/users/sign_up)

## MEGA65

![mega65-computer](https://mega65.org/assets/images/portfolio/grid-portfolio1.jpg)

La MEGA65 es como una C64 pero mejorada

### CPU

La MEGA65 usa el `45GS02`.
Que es una mejora del `CSG 4510` usado en la Commodore 65.
Que es una mejora del `65CE02`... que a su vez es una mejora del `65C02`, que a su vez es una mejora del `6502` usado por la Commodore 64.

Basicamente es un 6502 con la siguientes mejoras:

* Mas registros: aparte de `A`, `X` e `Y`, tambien tiene `Z`
* El registro `Q` es un registro virtual que sirve para acceder a `A`, `X`, `Y` y `Z` a la vez.
  * Esto permite tener operaciones de 32 bits!
* Direccionamiento de 32-bits
* Corre a 40Mhz (vs los 3.5Mhz de la C65, vs el 1Mhz de la C64)
* Tiene DMA

Para mas info: [mega65-chipset-reference.pdf][chipset-reference]

[chipset-reference]: https://files.mega65.org/manuals-upload/mega65-chipset-reference.pdf

### Graficos

La MEGA65 viene con el VIC-IV, que es una mejora del VIC-III usado por la Commodore 65.
Que a su vez es una mejora del VIC-II usado por la Commodore 64.

La VIC-IV soporta todos los modos graficos de la VIC-III y la VIC-II y ademas agrega algunos mas.

* 320x200x2 hi-res (VIC-II)
* 160x200x4 multi-color (VIC-II)
* 40x25 text, charset of 256 (VIC-II)
* 320x200x256 (VIC-III, 8 bitplane)
* 640x200x16 (VIC-III, 4 bitplanes)
* 640x400x16 (VIC-III, 4 bitplanes)
* [NOT SUPPORTED] 1280x200x4 (VIC-III, 2 bitplanes)
* [NOT SUPPORTED] 1280x400x4 (VIC-III, 2 bitplanes)
* 720x576 (VIC-IV)
* 800x600 (VIC-IV)

### Charset

* Charset soportados por la C64
* Charset: Full-Color (256 colores), 64 bytes por char, charset the 8192 chars
* Charset: Nybble-color (16 colores), 32 bytes per char. Cada char tiene 16-bit the ancho (ojo!)

### Sonido

* Sonido: 4 SIDs, y OPL 3

Lectura recomendada:

* [MEGA65 book][mega65-book]

[mega65-book]: https://files.mega65.org/manuals-upload/

## Ejemplos, editores y mas

Ya que la MEGA65 soporta todos graficos/sonido/charset de la C64, los editores de la C64 se pueden usar para la MEGA65.

Pero no le estariamos "sacando el jugo" a la MEGA65 (no hay obligacion de "sacarle el jugo", pero si se puede, mejor)

### Emulador

* https://github.lgb.hu/xemu/
* [Instrucciones de como usarlo](https://gist.github.com/ricardoquesada/9f6cf7bef3e6952d21ee792524a57aaa)

### Editores

* [C64Studio][c64studio] soporta tiles MEGA65 (experimental!)
* [Tilemizer][tilemizer]: Convierte un .bmp a charset (anda bien!)
* [Aseparse65][aseparse65]: Script que convierte format Asesprite a charset (no me anduvo)
* [TRSE][trse]: IDE con soporte para MEGA65 (no soporta modos graficos de VIC-IV)
* [Aseprite][aseprite]: muy buen pixel-art editor, pago
* [Discusion sobre que editores usar][editores]

[c64studio]: https://www.georg-rottensteiner.de/webmisc/C64StudioRelease.zip
[tilemizer]: https://github.com/JettMonstersGoBoom/Mega65
[aseparse65]: https://github.com/smnjameson/M65_Examples/tree/main/_include/aseparse65
[trse]: https://lemonspawn.com/turbo-rascal-syntax-error-expected-but-begin/
[aseprite]: https://www.aseprite.org/
[editores]: https://itch.io/jam/the-big-mega65-christmas-competition/topic/1663232/available-character-and-tile-editors-sprite-editors

### Ejemplos

* [M65_Examples][m65_examples]: DMA, Nybble Mode, Raster Rewrite Buffer en asm
* [Megadots][megadots]: jueguito en asm
* [Mega65-skip][mega65-ski]: jueguito en millfork

[m65_examples]: https://github.com/smnjameson/M65_Examples
[megadots]: https://github.com/jaammees/megadots
[mega65-ski]: https://github.com/jaammees/mega-65-ski

### Ensambladores

* [ACME][acme]: asm, soporta los nuevos opcodes de la MEGA65
* [KickC][kickc]: C + asm, soporta los nuevos opcodes la MEGA65
* [KickAss][kickass]: asm, version modificada para soportar opcodes de la MEGA65
* [CC65][cc65]: C + asm, detecta la CPU de la MEGA65, pero no soporta los opcodes nuevos

Macros:

* [ACME M65 Macros][acme_macros]: Algunas macros utiles para ACME + M65
* [KickAss M65 Macros][kickass_macros]: Macros utiles para KickC/KickAss

[acme]: https://sourceforge.net/projects/acme-crossass/
[kickc]: https://gitlab.com/camelot/kickc/
[kickass]: https://gitlab.com/camelot/kickc/-/tree/master/repo/cml/kickass/kickassembler/5.16-65ce02.e
[cc65]: https://github.com/cc65/cc65
[acme_macros]: test/macros.s
[kickass_macros]: https://github.com/smnjameson/M65_KickAsm_Macros

### Tutoriales

* Usa Aseprite + ASM para crear un engine de plaformas: https://www.youtube.com/watch?v=3xMbCHCy5Nk&list=PLq4NVS62WsPDHfWUaJ2L_Az_mlSrJ9mjg
