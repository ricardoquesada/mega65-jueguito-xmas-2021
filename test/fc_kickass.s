// ;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
.cpu _45gs02
#import "m65macros_kickass.s"

// ;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
// ; Start
BasicUpstart65(Entry)
* = $2016 "Basic Entry"

*=$2020

Entry:
	sei
	lda #$35
	sta $01

	enable40Mhz()
	enableVIC4Registers()

	lda #$00
	sta $d020
	sta $d021

	// ; Set to 40 columns by disabling H640 (bit 7) of $d031
	lda #$80
	trb $d031

	// ; SEAM (Super-Extended Attribute Mode)
	lda #$05
	sta $d054

	// ; Skip 80 chars per row, and not 40, since we are using 2-bytes per char
	lda #80
	sta $d058
	lda #0
	sta $d059

	// ; Number of chars to display per row
	lda #40
	sta $d05e


	ldx #$00
l1:
	lda CHRMAP+256*0,x
	sta $0800+256*0,x
	lda CHRMAP+256*1,x
	sta $0800+256*1,x
	lda CHRMAP+256*2,x
	sta $0800+256*2,x
	lda CHRMAP+256*3,x
	sta $0800+256*3,x
	lda CHRMAP+256*4,x
	sta $0800+256*4,x
	lda CHRMAP+256*5,x
	sta $0800+256*5,x
	lda CHRMAP+256*6,x
	sta $0800+256*6,x
	lda CHRMAP+256*7,x
	sta $0800+256*7,x

	lda COLORMAP+256*0,x
	sta $D800+256*0,x
	lda COLORMAP+256*1,x
	sta $D800+256*1,x
	lda COLORMAP+256*2,x
	sta $D800+256*2,x
	lda COLORMAP+256*3,x
	sta $D800+256*3,x
	// lda COLORMAP+256*4,x
	// sta $D800+256*4,x
	// lda COLORMAP+256*5,x
	// sta $D800+256*5,x
	// lda COLORMAP+256*6,x
	// sta $D800+256*6,x
	// lda COLORMAP+256*7,x
	// sta $D800+256*7,x

	inx
	bne l1

	// ; enable C64 MCM
	// ; this allows us to use multiple palettes
	// lda $D016
	// ora #$10
	// sta $D016

	ldx #$00
l2:
	// ; color map
	lda COLORS+256*0,x		// Red
	sta $D100+256*0,x
	lda COLORS+256*1,x 		// Green
	sta $D100+256*1,x
	lda COLORS+256*2,x 		// Blue
	sta $D100+256*2,x
	inx
	bne l2

keywait:
	lda $d610
	beq keywait
	sta $d610			// write anything to $d610 to pop key press from queue
					// A contains key press in ascii (not petscii)

	cli
	rts

COLORS:
	.import binary "data/lost_vickings.clut"
CHRMAP:
	.import binary "data/lost_vickings.map"
COLORMAP:
	.import binary "data/lost_vickings.atr"
* = $8000 "Chars"
	.import binary "data/lost_vickings.chrs"
