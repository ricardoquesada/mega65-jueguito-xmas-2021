; testing MEGA65
!cpu m65
!to "bin/fc_acme.prg", cbm
!sl "bin/fc_acme_labels.txt"


;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
!source "m65macros_acme.s"

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
; Start
	*=$2001
!zone
start:
	!word	$2009
	!word	2021				; Line 2021
	!byte	$fe, $02, $30, $00		; "BANK 0"
	!word	$2013
	!word	2022				; Line 2022
	!raw	$9e,"8213"			; "SYS8213"
	!byte	0,0,0

	sei
	lda #$35
	sta $01

	+ENABLE_40MHZ
	+ENABLE_VIC4
;	+VIC4_SET_CHAR_ADDR $8000		; $1000 Upper case. $1800 lower case
;	+VIC4_SET_SCREEN_ADDR $0800		; default


	lda #$00
	sta $d020
	sta $d021

	; Set to 40 columns by disabling H640 (bit 7) of $d031
	lda #$80
	trb $d031

	; SEAM (Super-Extended Attribute Mode)
	lda #$05
	sta $d054

	; Skip 80 chars per row, and not 40, since we are using 2-bytes per char
	lda #80
	sta $d058
	lda #0
	sta $d059

	; Number of chars to display per row
	lda #40
	sta $d05e

	ldx #$00
.l1:
	lda CHRMAP+256*0,x
	sta $0800+256*0,x
	lda CHRMAP+256*1,x
	sta $0800+256*1,x
	lda CHRMAP+256*2,x
	sta $0800+256*2,x
	lda CHRMAP+256*3,x
	sta $0800+256*3,x
	lda CHRMAP+256*4,x
	sta $0800+256*4,x
	lda CHRMAP+256*5,x
	sta $0800+256*5,x
	lda CHRMAP+256*6,x
	sta $0800+256*6,x
	lda CHRMAP+256*7,x
	sta $0800+256*7,x

	lda COLORMAP+256*0,x
	sta $D800+256*0,x
	lda COLORMAP+256*1,x
	sta $D800+256*1,x
	lda COLORMAP+256*2,x
	sta $D800+256*2,x
	lda COLORMAP+256*3,x
	sta $D800+256*3,x
	// lda COLORMAP+256*4,x
	// sta $D800+256*4,x
	// lda COLORMAP+256*5,x
	// sta $D800+256*5,x
	// lda COLORMAP+256*6,x
	// sta $D800+256*6,x
	// lda COLORMAP+256*7,x
	// sta $D800+256*7,x

	inx
	bne .l1

	; enable C64 MCM
	; this allows us to use multiple palettes
	// lda $D016
	// ora #$10
	// sta $D016

	ldx #$00
.l2:
	; color map
	lda COLORS+256*0,x		; Red
	sta $D100+256*0,x
	lda COLORS+256*1,x 		; Green
	sta $D100+256*1,x
	lda COLORS+256*2,x 		; Blue
	sta $D100+256*2,x
	inx
	bne .l2

keywait:
	lda $d610
	beq keywait
	sta $d610			; write anything to $d610 to pop key press from queue
					; A contains key press in ascii (not petscii)

	cli
	rts

COLORS:
	!binary "data/lost_vickings.clut"
CHRMAP:
	!binary "data/lost_vickings.map"
COLORMAP:
	!binary "data/lost_vickings.atr"
* = $8000
	!binary "data/lost_vickings.chrs"
