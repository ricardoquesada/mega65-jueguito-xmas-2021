; testing MEGA65
!cpu m65
!to "test.prg", cbm
!sl "labels.txt"


;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
!source "macros.s"

;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
; Start
	*=$2001
!zone
start:
	!word	$2009
	!word	2021				; Line 2021
	!byte	$fe, $02, $30, $00		; "BANK 0"
	!word	$2013
	!word	2022				; Line 2022
	!raw	$9e,"8213"			; "SYS8213"
	!byte	0,0,0

	+ENABLE_40MHZ
	+ENABLE_VIC4
	+VIC4_SET_CHAR_ADDR $1000		; $1000 Upper case. $1800 lower case
	+VIC4_SET_SCREEN_ADDR $0800		; default

	lda #$ea
	sta $4000

	lda #$00
	sta $d020
	sta $d021

	; Set to 40 columns by disabling H640 (bit 7) of $d031
	lda #$80
	trb $d031

	; lda #$05    //Enable 16 bit char numbers (bit0) and
	; sta $d054   //full color for chars>$ff (bit2)
	rts
