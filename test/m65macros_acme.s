;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
; Macros
; Converted from KickAss to ACME.
; Original:
; https://github.com/smnjameson/M65_Examples/blob/main/_include/m65macros.s
;=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
!macro ENABLE_40MHZ {
		lda #$41
		sta $00             ; 40 Mhz mode
}

!macro ENABLE_VIC4 {
		lda #$00
		tax
		tay
		taz
		map
		eom

		lda #$47            ; Enable VIC IV
		sta $d02f
		lda #$53
		sta $d02f
}

!macro ENABLE_VIC3 {
		lda #$00
		tax
		tay
		taz
		map
		eom

		lda #$A5            ; Enable VIC III
		sta $d02f
		lda #$96
		sta $d02f
}

!macro DISABLE_C65_ROM {
		; Disable C65 rom protection using
		; hypervisor trap (see mega65 manual)
		lda #$70
		sta $d640
		eom
		; Unmap C65 Roms $d030 by clearing bits 3-7
		lda #%11111000
		trb $d030
}

; To use the additional memory for screen RAM, the screen RAM start address can be
; adjusted to any location in memory with byte-level granularity by setting the SCRNPTR
; registers ($D060 – $D063, 53344 – 53347 decimal)
!macro VIC4_SET_SCREEN_ADDR .addr {
		lda #((.addr) & $ff)
		sta $d060
		lda #(((.addr) & $ff00)>>8)
		sta $d061
		lda #(((.addr) & $ff0000)>>16)
		sta $d062
		; FIXME: Should $d063 be updated (???)
}

; The area of colour RAM being used can be similarly set using the COLPTR registers
; ($D064 – $D065, 53348 – 53349 decimal). That is, the value is an offset from the
; start of the colour / attribute RAM. This is because, like on the C64, the colour /
; attribute RAM of the MEGA65 is a separate memory component, with its own dedicated
; connection to the VIC-IV. By default, the COLPTRs are set to zero, which replicates
; the behaviour of the VIC-II/II
!macro VIC4_SET_ATTRIB_ADDR .addr {
		lda #((.addr) & $ff)
		sta $d064
		lda #(((.addr) & $ff00)>>8)
		sta $d065
}

; The location of the character generator data can also be set with byte-level precision
; via the CHARPTR registers at $D068 – $D06A (53352 – 53354 decimal). As usual,
; the first of these registers holds the lowest-order byte, and the last the highest-order
; byte. The three bytes allow for placement of character data anywhere in the first
; 16MB of RAM. For systems with less than 16MB of RAM accessible by the VIC-IV, the
; upper address bits should be zero.
!macro VIC4_SET_CHAR_ADDR .addr {
		lda #((.addr) & $ff)
		sta $d068
		lda #(((.addr) & $ff00)>>8)
		sta $d069
		lda #(((.addr) & $ff0000)>>16)
		sta $d06a
}

