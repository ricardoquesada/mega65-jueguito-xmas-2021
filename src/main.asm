StartBlock0:
	; Starting new memory block at 0
.p4510   ; 65816 processor
;.i16    ; X/Y are 16 bits
;.a8     ; A is 8 bits
;.segment "CODE"
EndBlock0:
StartBlock2001:
	; Starting new memory block at $2001
	 .org $2001
	 .byte $09,$20 ;End of command marker (first byte after the 00 terminator)
	 .byte $0a,$00 ;10
	 .byte $fe,$02,$30,$00 ;BANK 0
	 .byte $13, $20 
	 .byte $14,$00 ;20
	 .byte $9e ;SYS
	 .byte $38,$32,$32,$34
	  .byte $00
endd_s:
	  .byte $00,$00    ;End of basic terminators
	  .byte $FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF,$FF    ;extra
	; Ending memory block at $2001
	; Resuming memory block at $2001
HelloWorld:
	; LineNumber: 6
	jmp block1
	; LineNumber: 5
Memory_zpLo	= $90
	; LineNumber: 6
Memory_zpHi	= $92
	; NodeProcedureDecl -1
	; ***********  Defining procedure : init16x8mul
	;    Procedure type : Built-in function
	;    Requires initialization : no
mul16x8_num1Hi = $4C
mul16x8_num1 = $4E
mul16x8_num2 = $50
mul16x8_procedure:
	lda #$00
	ldy #$00
	beq mul16x8_enterLoop
mul16x8_doAdd:
	clc
	adc mul16x8_num1
	tax
	tya
	adc mul16x8_num1Hi
	tay
	txa
mul16x8_loop:
	asl mul16x8_num1
	rol mul16x8_num1Hi
mul16x8_enterLoop:
	lsr mul16x8_num2
	bcs mul16x8_doAdd
	bne mul16x8_loop
	rts
	; NodeProcedureDecl -1
	; ***********  Defining procedure : initeightbitmul
	;    Procedure type : Built-in function
	;    Requires initialization : no
multiplier = $4C
multiplier_a = $4E
multiply_eightbit:
	cpx #$00
	beq mul_end
	dex
	stx $4E
	lsr
	sta multiplier
	lda #$00
	ldx #$08
mul_loop:
	bcc mul_skip
mul_mod:
	adc multiplier_a
mul_skip:
	ror
	ror multiplier
	dex
	bne mul_loop
	ldx multiplier
	rts
mul_end:
	txa
	rts
initeightbitmul_multiply_eightbit2:
	rts
	;*
; //Sets the column mode to 40(320x200)
; //
; 

	;*
; //The VIC4 is hidden by default on the mega65. Use this method to make it visible for regular pokes. 
; 

	; NodeProcedureDecl -1
	; ***********  Defining procedure : Screen_EnableVIC4
	;    Procedure type : User-defined procedure
	; LineNumber: 117
Screen_EnableVIC4:
	; LineNumber: 118
	; ****** Inline assembler section
		lda #$47  
        sta $d02f
        lda #$53
        sta $d02f
	
	; LineNumber: 124
	rts
	;*
; //Initializes the screen of the mega65
; 

	; NodeProcedureDecl -1
	; ***********  Defining procedure : Screen_Init
	;    Procedure type : User-defined procedure
	; LineNumber: 142
Screen_Init:
	; LineNumber: 143
	; ****** Inline assembler section
	
         lda #$47
         sta $d02f
         lda #$53
         sta $d02f
         ; no interrupts
         sei
         
		
		 lda #$00
        tax 
        tay 
        taz 
        map
        eom
	
	; LineNumber: 163
	rts
	;*
; //Enables Super-extended attribute mode(SEAM)
; 

	; NodeProcedureDecl -1
	; ***********  Defining procedure : Screen_EnableSeamMode
	;    Procedure type : User-defined procedure
	; LineNumber: 168
Screen_EnableSeamMode:
	; LineNumber: 169
	; LineNumber: 42
	; LineNumber: 43
	; ****** Inline assembler section
		lda $D031
		and #%01111111
		sta $d031
	
	; LineNumber: 48
	; LineNumber: 170
	; ****** Inline assembler section
	    lda #$5
        sta $d054
        lda #$50
        sta $d058
        lda #$00
        sta $d059
	
	; LineNumber: 178
	rts
block1:
	; LineNumber: 8
	
; // Initialize the screen + enable VIC4
	jsr Screen_Init
	; LineNumber: 9
	jsr Screen_EnableVIC4
	; LineNumber: 10
	; LineNumber: 42
	; LineNumber: 43
	; ****** Inline assembler section
		lda $D031
		and #%01111111
		sta $d031
	
	; LineNumber: 48
	; LineNumber: 11
	jsr Screen_EnableSeamMode
	; LineNumber: 12
	jmp * ; loop like (�/%
	; LineNumber: 15
	; End of program
	; Ending memory block at $2001
	; Ending memory block at $2001
EndBlock2001:
